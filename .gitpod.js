#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
/**
 * @param {string} template 
 * @param {*} params 
 * @returns 
 */
function interpolate(template, params) {
    if (params == null) {
        return template;
    }
    let result = String(template);
    Object.keys(params).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(params, key)) {
            result = result.replace(new RegExp('\\${' + key + '}', 'g'), String(params[key]).toString());
        }
    });
    return result;
}
// validate if app.development.json already exists
const developmentConfigurationPath = path.resolve(__dirname, 'src/assets/config/app.development.json');
const exists = fs.existsSync(developmentConfigurationPath);
if (exists === false) {
    // get template configuration
    const templateConfiguration = require(path.resolve(__dirname, 'src/assets/config/.gitpod.json'));
    const str = interpolate(JSON.stringify(templateConfiguration), Object.assign({}, process.env));
    const final = JSON.parse(str);
    fs.writeFileSync(developmentConfigurationPath, JSON.stringify(final, null, 4), 'utf8');
} else {
    process.stdout.write('INFO Gitpod workspace configuration already exists.');
}