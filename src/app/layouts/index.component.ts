import {Component, OnInit, ViewChild, Input, AfterViewInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {ConfigurationService, UserService, AppSidebarService} from '@universis/common';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [`
  :host {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-height: 100vh;
  }
  `]
})
export class IndexComponent implements AfterViewInit {

  constructor(private _userService: UserService,
              private _configurationService: ConfigurationService,
              private _appSidebarService: AppSidebarService,
              private _translateService: TranslateService
          ) { }

  @ViewChild('appSidebarNav') appSidebarNav: any;
  @Input() user: any;
  public today = new Date();
  public languages?: any;
  public currentLang?: string;
  public applicationImage?: string;
  public applicationTitle?: string;

  ngAfterViewInit() {
    
    this._userService.getUser().then((user) => {
      const sidebarLocations = (this._appSidebarService as any).sidebarLocations.slice();
      sidebarLocations.forEach((sidebarLocation: { name: string; children?: Array<any> }) => {
        sidebarLocation.name = this._translateService.instant(sidebarLocation.name);
        if (Array.isArray(sidebarLocation.children)) {
          sidebarLocation.children.forEach((item) => {
            item.name = this._translateService.instant(item.name);
          });
        }
      });
      // get sidebar navigation items
      this.appSidebarNav.navItemsArray = sidebarLocations;
      // get current user
      this.user = user;
      // get current language
      this.currentLang = this._configurationService.currentLocale;
      // get languages
      this.languages = this._configurationService.settings.i18n?.locales;
      // get path of brand logo
      const appSettings: any= this._configurationService.settings.app;
      this.applicationImage = appSettings && appSettings.image;
      // get application title
      this.applicationTitle = (appSettings && appSettings.title) || 'Universis';
    });

    
  }

  changeLanguage(lang: any) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }
}
