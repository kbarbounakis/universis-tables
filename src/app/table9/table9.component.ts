import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { QueryBuilderComponent, QueryBuilderService } from 'projects/ngx-query-builder/src/public-api';
import { AdvancedTableComponent, ActivatedTableService } from 'projects/ngx-tables/src/public_api';

@Component({
  selector: 'app-table9',
  templateUrl: './table9.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class Table9Component implements AfterViewInit {

  @ViewChild('table') table?: AdvancedTableComponent;
  public modalRef?: BsModalRef;
  constructor(private _activatedTableService: ActivatedTableService,
    private modalService: BsModalService,
    private queryBuilderService: QueryBuilderService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }
  public filter: any = {};
  public queryDescription?: string;
  public query: any = {
    type: 'LogicalExpression',
    operator: '$and',
    elements: [
      {
        type: 'BinaryExpression',
        operator: '$eq',
        left: null,
        right: null
      }
    ]
  };


  ngAfterViewInit() {
    this._activatedTableService.activeTable = this.table;
  }

  showBuilder(builderTemplate: any) {
    const initialState: ModalOptions = {
      class: 'modal-xl query-builder-container',
      initialState: {
      }
    };
    this.modalRef = this.modalService.show(builderTemplate, initialState);
  }

  closeBuilder() {
    this.modalRef?.hide();
  }

  applyQuery(builder: QueryBuilderComponent) {
    this.modalRef?.hide();
    const queryParams = builder.queryParams;
    this.router.navigate([ '.' ], {
      queryParams: {
        $filter: queryParams.$filter
      },
      skipLocationChange: true,
      relativeTo: this.activatedRoute
    }).then(() => {
      this.queryDescription = builder.queryDescription;
      this.table?.fetch(true);
    });
  }
  
}
