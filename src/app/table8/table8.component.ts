import { AfterViewInit, Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { AdvancedTableComponent, ActivatedTableService, AdvancedSearchFormComponent } from 'projects/ngx-tables/src/public_api';
import { getLocaleDateFormat, getLocaleDateTimeFormat, FormatWidth } from '@angular/common'
@Component({
  selector: 'app-table8',
  templateUrl: './table8.component.html',
  styles: []
})
export class Table8Component implements AfterViewInit {

  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('searchForm') searchForm?: AdvancedSearchFormComponent;
  constructor(private _activatedTableService: ActivatedTableService, @Inject(LOCALE_ID) private locale: string) { }
  public filter: any = {};

  ngAfterViewInit() {
    this._activatedTableService.activeTable = this.table;
  }

  addItem() {
    alert('Default action clicked');
  }

  onLoadingTable(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // alter column
      let column = event.target.config.columns.find((item) => item.name === 'semester');
      if (column) {
        column.formatter = 'NgClassFormatter';
        column.formatOptions = {
          ngClass: {
            "text-success font-weight-bold font-lg": "${semester} <= 10"
          }
        }
      }
      // add column
      column = event.target.config.columns.find((item) => item.name === 'person/birthDate');
      if (column == null) {
        event.target.config.columns.push(
          {
            "name":"person/birthDate",
            "property": "birthDate",
            "title":"Birth Date",
            "formatter": "DateTimeFormatter",
            "formatString":"shortDate"
          }
        );
        // add filter
        event.target.config.criteria.push({
          "name": "birthDate",
          "filter": "(person/birthDate eq '${value.substring(0,10)}')",
          "type": "text"
        })
      }
    }
  }

  onLoadingSearch(event: { target: AdvancedSearchFormComponent }) {
    const form = event && event.target && event.target.form;
    if (form && form.components) {
      //
      const container = form.components[0].columns;
      const birthDateComponentIndex = container.findIndex((item: any) => item.components && item.components[0].label === 'Birth Date');
      if (birthDateComponentIndex < 0) {
        const searchNameComponentIndex = container.findIndex((item: any) => item.components && item.components[0].label === 'Name');
        if (searchNameComponentIndex >= 0) {
          // get format for current date
          let format = getLocaleDateFormat(this.locale, FormatWidth.Short);
          // replace yy with yyyy (this is very important for a birth date)
          if (/yyyy/i.test(format) === false) {
            format = format.replace(/yy/i, 'yyyy');
          }
          container.splice(searchNameComponentIndex + 1, 0, {
            "components": [
              {
                "label": "Birth Date",
                "useLocaleSettings": true,
                "format": format,
                "hideLabel": false,
                "tableView": false,
                "datePicker": {
                  "disableWeekends": false,
                  "disableWeekdays": false
                },
                "enableTime": false,
                "enableMinDateInput": false,
                "enableMaxDateInput": false,
                "key": "birthDate",
                "type": "datetime",
                "input": true,
                "widget": {
                  "type": "calendar",
                  "displayInTimezone": "viewer",
                  "locale": "en",
                  "useLocaleSettings": true,
                  "allowInput": true,
                  "mode": "single",
                  "enableTime": false,
                  "noCalendar": false,
                  "format": format,
                  "hourIncrement": 1,
                  "minuteIncrement": 1,
                  "time_24hr": false,
                  "minDate": null,
                  "disableWeekends": false,
                  "disableWeekdays": false,
                  "maxDate": null
                }
              }
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          });
        }
      }
    }
  }

}
